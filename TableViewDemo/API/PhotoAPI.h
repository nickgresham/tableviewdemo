//
//  PhotoAPI.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 8/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Photo.h"

@protocol PhotoAPIDelegate;

@interface PhotoAPI : NSObject

@property(nonatomic, weak) id <PhotoAPIDelegate> delegate;

- (void) getPhotos;

@end

@protocol PhotoAPIDelegate <NSObject>

- (void)photoAPI:(PhotoAPI *)api updatedPhotos:(NSArray<Photo *> *)photos;

@end