//
//  PhotoAPI.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 8/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "PhotoAPI.h"
#import <AFNetworking.h>

static NSString *const kPAUrl = @"https://jsonplaceholder.typicode.com/photos";

@interface PhotoAPI ()
@property(nonatomic) AFHTTPSessionManager *httpSessionManager;
@end

@implementation PhotoAPI

#pragma mark - API methods

- (void)getPhotos {
    [self.httpSessionManager GET:kPAUrl
                      parameters:nil
                        progress:nil
                         success:^(NSURLSessionTask *task, id responseObject) {
                             NSMutableArray *newPhotos = [NSMutableArray new];

                             for (NSDictionary *jsonDictionary in responseObject) {
                                 NSURL *url = [NSURL URLWithString:jsonDictionary[@"url"]];
                                 NSURL *thumbnailUrl = [NSURL URLWithString:jsonDictionary[@"thumbnailUrl"]];
                                 Photo *photo = [[Photo alloc] initWithId:jsonDictionary[@"id"]
                                                                  albumId:jsonDictionary[@"albumId"]
                                                                    title:jsonDictionary[@"title"]
                                                                      url:url
                                                             thumbnailUrl:thumbnailUrl];
                                 [newPhotos addObject:photo];
                             }
                             if (self.delegate != nil) {
                                 [self.delegate photoAPI:self updatedPhotos:newPhotos];
                             }
                         }
                         failure:^(NSURLSessionTask *operation, NSError *error) {
                             NSLog(@"Error: %@", error);
                         }];
}

- (AFHTTPSessionManager *)httpSessionManager {
    if (!_httpSessionManager) {
        _httpSessionManager = [AFHTTPSessionManager manager];
    }
    return _httpSessionManager;
}

@end
