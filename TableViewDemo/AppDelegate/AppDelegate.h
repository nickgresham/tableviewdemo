//
//  AppDelegate.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 7/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;


@end

