//
//  Photo.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 8/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Photo : NSObject

@property(nonatomic) NSNumber *id;
@property(nonatomic) NSNumber *albumId;
@property(nonatomic) NSString *title;
@property(nonatomic) NSURL *url;
@property(nonatomic) NSURL *thumbnailUrl;

- (id)initWithId:(NSNumber *)id
         albumId:(NSNumber *)albumId
           title:(NSString *)title
             url:(NSURL *)url
    thumbnailUrl:(NSURL *)thumbnailUrl;

@end
