//
//  Photo.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 8/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "Photo.h"

@implementation Photo

- (id)init {
    return nil;
}

- (id)initWithId:(NSNumber *)id
         albumId:(NSNumber *)albumId
           title:(NSString *)title
             url:(NSURL *)url
    thumbnailUrl:(NSURL *)thumbnailUrl {
    self = [super init];
    if (self) {
        self.id = id;
        self.albumId = id;
        self.title = title;
        self.url = url;
        self.thumbnailUrl = thumbnailUrl;
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Photo: id '%@' albumID '%@' title '%@' url: '%@' thumbnailUrl: '%@'",
                                      self.id, self.albumId, self.title, self.url, self.thumbnailUrl];
}

@end
