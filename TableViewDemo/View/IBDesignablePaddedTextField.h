//
//  IBDesignablePaddedTextField.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 7/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//
//  A simple IBDesignable for adding padding to both sides of UITextFields, useful for fields with opaque backgrounds
//  without borders

#import <UIKit/UIKit.h>

@interface IBDesignablePaddedTextField : UITextField

@property(nonatomic) IBInspectable CGFloat padding;

@end
