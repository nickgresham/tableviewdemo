//
//  IBDesignablePaddedTextField.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 7/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "IBDesignablePaddedTextField.h"

IB_DESIGNABLE
@implementation IBDesignablePaddedTextField

#pragma mark - Object Creation

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self inspectableDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (!self) {
        [self inspectableDefaults];
    }
    return self;
}

#pragma mark - UITextField Overrides

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect rectangle = [super textRectForBounds:bounds];
    UIEdgeInsets insets = UIEdgeInsetsMake(0, _padding, 0, _padding);
    return UIEdgeInsetsInsetRect(rectangle, insets);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect rectangle = [super editingRectForBounds:bounds];
    UIEdgeInsets insets = UIEdgeInsetsMake(0, _padding, 0, _padding);
    return UIEdgeInsetsInsetRect(rectangle, insets);
}

#pragma mark - Interface Builder

- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
}

- (void)inspectableDefaults {
    _padding = 0;
}
@end
