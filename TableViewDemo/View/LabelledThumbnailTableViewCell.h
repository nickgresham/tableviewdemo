//
//  LabelledThumbnailTableViewCell.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 11/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelledThumbnailTableViewCell : UITableViewCell

- (void)configureWithImageUrl:(NSURL *)imageUrl
                        title:(NSString *)title
                     subtitle:(NSString *)subtitle
                      content:(NSString *)content;

@end
