//
//  LabelledThumbnailTableViewCell.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 11/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "LabelledThumbnailTableViewCell.h"
#import <UIImageView+AFNetworking.h>

@interface LabelledThumbnailTableViewCell ()

@property(weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation LabelledThumbnailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithImageUrl:(NSURL *)imageURL
                        title:(NSString *)title
                     subtitle:(NSString *)subtitle
                      content:(NSString *)content {
    [_thumbnailImageView setImageWithURL:imageURL
                        placeholderImage:[UIImage imageNamed:@"Icon_Thumbnail_Placeholder"]];
    _titleLabel.text = title;
    _subtitleLabel.text = subtitle;
    _contentLabel.text = content;
}

@end
