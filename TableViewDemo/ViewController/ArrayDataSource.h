//
//  ArrayDataSource.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 11/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^TableViewCellConfigureBlock)(id cell, id item);

@interface ArrayDataSource : NSObject <UITableViewDataSource>

@property(nonatomic, readonly) NSMutableArray *items;

- (id)initWithItems:(NSArray *)items
     cellIdentifier:(NSString *)cellIdentifier
 configureCellBlock:(TableViewCellConfigureBlock)configureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

- (void)setItemsTo:(NSArray *)items;

@end
