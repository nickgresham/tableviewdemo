//
//  ArrayDataSource.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 11/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "ArrayDataSource.h"

@interface ArrayDataSource ()

@property(nonatomic, copy) NSString *cellIdentifier;
@property(nonatomic, copy) TableViewCellConfigureBlock configureCellBlock;
@property(nonatomic) NSMutableArray *deletedRows;

@end


@implementation ArrayDataSource

#pragma mark - Init

- (id)init {
    return nil;
}

- (id)initWithItems:(NSArray *)items
     cellIdentifier:(NSString *)cellIdentifier
 configureCellBlock:(TableViewCellConfigureBlock)configureCellBlock {
    self = [super init];
    if (self) {
        _items = [NSMutableArray arrayWithArray:items];
        self.cellIdentifier = cellIdentifier;
        self.configureCellBlock = [configureCellBlock copy];
        self.deletedRows = [NSMutableArray new];
    }
    return self;
}

#pragma Getters and Setters

- (void)setItemsTo:(NSArray *)items {
    _items = [NSMutableArray arrayWithArray:items];
    for (NSIndexPath *indexPath in self.deletedRows) {
        if (_items.count > indexPath.row) {
            [_items removeObjectAtIndex:indexPath.row];
        }
    }
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return self.items[(NSUInteger) indexPath.row];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier
                                                            forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    self.configureCellBlock(cell, item);
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.deletedRows addObject:indexPath];
        [self.items removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


@end
