//
//  LoginViewController.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 8/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)login:(id)sender {
    [self.view endEditing:YES];
}


@end
