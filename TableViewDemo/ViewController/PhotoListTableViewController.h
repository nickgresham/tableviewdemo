//
//  PhotoListTableViewController.h
//  TableViewDemo
//
//  Created by Nicholas Gresham on 11/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoListTableViewController : UITableViewController

@end
