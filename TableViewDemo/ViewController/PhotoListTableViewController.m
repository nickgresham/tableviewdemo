//
//  PhotoListTableViewController.m
//  TableViewDemo
//
//  Created by Nicholas Gresham on 11/4/18.
//  Copyright © 2018 Nicholas Gresham. All rights reserved.
//

#import "PhotoListTableViewController.h"
#import "ArrayDataSource.h"
#import "LabelledThumbnailTableViewCell.h"
#import "PhotoAPI.h"

static NSString *const kPLPhotoCellIdentifier = @"LabelledThumbnailCell";


@interface PhotoListTableViewController () <PhotoAPIDelegate, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>

@property(nonatomic) ArrayDataSource *photosArrayDataSource;
@property(nonatomic) ArrayDataSource *searchArrayDataSource;
@property(nonatomic) PhotoAPI *photoAPI;
@property(weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property(nonatomic) UISearchController *searchController;
@property(nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end

@implementation PhotoListTableViewController

#pragma mark ViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    [self setupSearch];
    [self setupPhotoAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Setup

- (void)setupTableView {
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.editButtonItem.enabled = NO;

    TableViewCellConfigureBlock configureCell = ^(LabelledThumbnailTableViewCell *cell, Photo *photo) {
        [cell configureWithImageUrl:photo.thumbnailUrl
                              title:photo.title
                           subtitle:[photo.albumId stringValue]
                            content:photo.title];
    };
    self.photosArrayDataSource = [[ArrayDataSource alloc] initWithItems:@[]
                                                         cellIdentifier:kPLPhotoCellIdentifier
                                                     configureCellBlock:configureCell];
    self.searchArrayDataSource = [[ArrayDataSource alloc] initWithItems:@[]
                                                         cellIdentifier:kPLPhotoCellIdentifier
                                                     configureCellBlock:configureCell];
    self.tableView.dataSource = self.photosArrayDataSource;

    self.tableView.rowHeight = UITableViewAutomaticDimension;

    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.color = [UIColor colorNamed:@"AppTint"];
    self.tableView.backgroundView = activityIndicatorView;
    activityIndicatorView.hidesWhenStopped = YES;
    [activityIndicatorView startAnimating];
    self.activityIndicatorView = activityIndicatorView;
}

- (void)setupSearch {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.navigationItem.titleView = self.searchController.searchBar;
}

- (void)setupPhotoAPI {
    self.photoAPI = [PhotoAPI new];
    self.photoAPI.delegate = self;
    [self.photoAPI getPhotos];
}

#pragma mark PhotoAPIDelegate

- (void)photoAPI:(PhotoAPI *)api updatedPhotos:(NSArray<Photo *> *)photos {
    [self.photosArrayDataSource setItemsTo:photos];
    [self.searchArrayDataSource setItemsTo:photos];
    [self.activityIndicatorView stopAnimating];
    [self.tableView reloadData];
    self.refreshButton.enabled = YES;
    self.editButtonItem.enabled = YES;
}

#pragma mark Action

- (IBAction)refresh:(id)sender {
    [self.photosArrayDataSource setItemsTo:@[]];
    [self.tableView reloadData];
    self.refreshButton.enabled = NO;
    [self.photoAPI getPhotos];
    [self.activityIndicatorView startAnimating];
}

#pragma mark UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

- (void)searchForText:(NSString *)searchText {
    NSMutableArray *items = self.photosArrayDataSource.items;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", searchText];
    NSArray *filtered = ![searchText isEqualToString:@""] ? [items filteredArrayUsingPredicate:predicate] : items;
    [self.searchArrayDataSource setItemsTo:filtered];
}

#pragma mark UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchForText:searchText];
    [self.tableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.tableView.dataSource = self.searchArrayDataSource;
    [self.searchArrayDataSource setItemsTo:self.photosArrayDataSource.items];
    self.editButtonItem.enabled = NO;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.tableView.dataSource = self.photosArrayDataSource;
    [self.tableView reloadData];
    self.editButtonItem.enabled = YES;
}

#pragma mark UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tableView.editing) {
        return UITableViewCellEditingStyleDelete;
    }

    return UITableViewCellEditingStyleNone;
}

@end
